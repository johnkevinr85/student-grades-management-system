Rails.application.routes.draw do
  resources :subjects do 
    member do
      get :edit_grade
      put :update_grade
    end
  end
  resources :students

  post '/enroll_student' => 'subjects#enroll'
  delete '/unenroll_student' => 'subjects#unenroll'

  root to: "students#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
