class Subject < ActiveRecord::Base
	has_many :student_subjects, :dependent => :destroy
	has_many :students, :through => :student_subjects

	validates_presence_of :title
	validates_presence_of :subject_code
	validates_presence_of :teacher
end
