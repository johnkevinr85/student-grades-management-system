class Student < ActiveRecord::Base
	has_many :student_subjects, :dependent => :destroy
	has_many :subjects, :through => :student_subjects

	validates_presence_of :name
	validates_format_of :birthday, :with => /\d{4}-\d{2}-\d{2}/
	validates_format_of :phone_number, :with => /09\d{9}/
	validates_format_of :email, :with => /[^\s@]+@[^\s@]+\.[^\s@]+/
	validates_presence_of :course
end
