class StudentSubject < ActiveRecord::Base
	belongs_to :subject
	belongs_to :student

	validates_presence_of :subject_id
	validates_presence_of :student_id
	validates :grade, numericality: { less_than_or_equal_to: 100, greater_than_or_equal_to: 65 }, allow_blank: true
end
