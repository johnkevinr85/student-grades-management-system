json.extract! student, :id, :name, :birthday, :email, :phone_number, :course, :created_at, :updated_at
json.url student_url(student, format: :json)
