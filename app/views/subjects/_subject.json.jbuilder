json.extract! subject, :id, :title, :subject_code, :teacher, :created_at, :updated_at
json.url subject_url(subject, format: :json)
