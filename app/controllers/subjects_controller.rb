class SubjectsController < ApplicationController
  before_action :set_subject, only: %i[ show edit update destroy edit_grade update_grade]
  before_action :set_students_available, only: %i[ show enroll]
  before_action :set_enrolled_students, only: %i[ show ] 
  before_action :set_student, only: %i[ edit_grade update_grade]

  # GET /subjects or /subjects.json
  def index
    @subjects = Subject.all
  end

  # GET /subjects/1 or /subjects/1.json
  def show
  end

  # GET /subjects/new
  def new
    @subject = Subject.new
  end

  # GET /subjects/1/edit
  def edit
  end

  # POST /subjects or /subjects.json
  def create
    @subject = Subject.new(subject_params)

    respond_to do |format|
      if @subject.save
        format.html { redirect_to subject_url(@subject), notice: "Subject was successfully created." }
        format.json { render :show, status: :created, location: @subject }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subjects/1 or /subjects/1.json
  def update
    respond_to do |format|
      if @subject.update(subject_params)
        format.html { redirect_to subject_url(@subject), notice: "Subject was successfully updated." }
        format.json { render :show, status: :ok, location: @subject }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subjects/1 or /subjects/1.json
  def destroy
    @subject.destroy

    respond_to do |format|
      format.html { redirect_to subjects_url, notice: "Subject was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def enroll
    @subject = Subject.find(params[:enrollment][:subject_id])
    @student = Student.find(params[:enrollment][:student_id])
    @enrollment = StudentSubject.new
    @enrollment.subject = @subject
    @enrollment.student = @student

    respond_to do |format|
      if @enrollment.save!
        format.html { redirect_to subject_url(@subject), notice: "Student enrolled successfully." }
        format.json { render :show, status: :created, location: @subject }
      else
        format.html { render :show, status: :unprocessable_entity }
        format.json { render json: @enrollment.errors, status: :unprocessable_entity }
      end
    end
  end

  def unenroll
    @student_subject = StudentSubject.where(student_id: params[:id], subject_id: params[:subject_id]).first
    @student_subject.destroy

    respond_to do |format|
      if params[:from_subjects] == "1"
        format.html { redirect_to subject_path(params[:subject_id]), notice: "Student unenrolled successfully." }
      else
        format.html { redirect_to student_path(params[:id]), notice: "Student unenrolled successfully." }
      end
      format.json { head :no_content }
    end
  end

  def edit_grade
  end

  def update_grade
    @record = StudentSubject.find_by_id(params[:record_id])

    respond_to do |format|
      if @record.update_attributes(grade: params[:grade])
        format.html { redirect_to subject_path(@subject), notice: "Grade updated successfully." }
      else
        format.html { render :edit_grade, status: :unprocessable_entity }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subject
      @subject = Subject.find(params[:id])
    end

    def set_enrolled_students
      @enrolled_students = Student.select('students.*, student_subjects.grade').joins(:student_subjects).where(student_subjects: {subject_id: params[:id]})
    end

    def set_students_available
      subject_id = params[:enrollment] || params[:id]
      enrolled_students = Student.joins(:student_subjects).where(student_subjects: {subject_id: subject_id})
      @students_available = Student.where.not(id: enrolled_students.pluck(:id))
    end

    def set_student
      @student = Student.select('students.*, student_subjects.grade, student_subjects.id as record_id').joins(:student_subjects).where(student_subjects: {student_id: params[:student_id]}).first
    end

    # Only allow a list of trusted parameters through.
    def subject_params
      params.require(:subject).permit(:title, :subject_code, :teacher)
    end
end
