class CreateStudentSubject < ActiveRecord::Migration[5.1]
  def change
    create_table :student_subjects do |t|
      t.integer :student_id
      t.integer :subject_id
      t.decimal :grade, precision: 3, scale: 2
    end
  end
end
