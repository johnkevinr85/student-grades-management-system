class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :name
      t.string :birthday
      t.string :email
      t.string :phone_number
      t.string :course

      t.timestamps
    end
  end
end
